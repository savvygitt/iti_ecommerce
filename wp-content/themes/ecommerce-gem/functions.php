<?php
/**
 * eCommerce Gem functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package eCommerce_Gem
 */

if ( ! function_exists( 'ecommerce_gem_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ecommerce_gem_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on eCommerce Gem, use a find and replace
	 * to change 'ecommerce-gem' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'ecommerce-gem', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 */
	add_theme_support( 'custom-logo' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size('ecommerce-gem-common', 370, 260, true);

	// Register navigation menu locations.
	register_nav_menus( array(
		'top' 		=> esc_html__( 'Top Header', 'ecommerce-gem' ),
		'primary' 	=> esc_html__( 'Primary Header', 'ecommerce-gem' ),
		'social'  	=> esc_html__( 'Social Links', 'ecommerce-gem' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ecommerce_gem_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add woocommerce support, gallery zoom, lightbox and thumbnail slider.
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );

	$gallery_zoom = ecommerce_gem_get_option( 'enable_gallery_zoom' );

	if( 1 == $gallery_zoom ){
		add_theme_support( 'wc-product-gallery-zoom' );
	}
}
endif;
add_action( 'after_setup_theme', 'ecommerce_gem_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ecommerce_gem_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ecommerce_gem_content_width', 810 );
}
add_action( 'after_setup_theme', 'ecommerce_gem_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ecommerce_gem_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'ecommerce-gem' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here to appear in Sidebar.', 'ecommerce-gem' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	if( class_exists( 'WooCommerce' ) ){

		register_sidebar( array(
			'name'          => esc_html__( 'Shop Sidebar', 'ecommerce-gem' ),
			'id'            => 'shop-sidebar',
			'description'   => esc_html__( 'Widgets added here will appear in sidebar of shop pages only.', 'ecommerce-gem' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );

	}

	register_sidebar( array(
		'name'          => esc_html__( 'Advertisements Below Slider', 'ecommerce-gem' ),
		'id'            => 'advertisement-area',
		'description'   => esc_html__( 'Add widgets here to appear in advertisement area below main slider of home page', 'ecommerce-gem' ),
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Home Page Widget Area', 'ecommerce-gem' ),
		'id'            => 'home-page-widget-area',
		'description'   => esc_html__( 'Add widgets here to appear in Home Page Widget Area.', 'ecommerce-gem' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="container">',
		'after_widget'  => '</div></section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => sprintf( esc_html__( 'Footer %d', 'ecommerce-gem' ), 1 ),
		'id'            => 'footer-1',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => sprintf( esc_html__( 'Footer %d', 'ecommerce-gem' ), 2 ),
		'id'            => 'footer-2',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => sprintf( esc_html__( 'Footer %d', 'ecommerce-gem' ), 3 ),
		'id'            => 'footer-3',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => sprintf( esc_html__( 'Footer %d', 'ecommerce-gem' ), 4 ),
		'id'            => 'footer-4',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ecommerce_gem_widgets_init' );

/**
* Enqueue scripts and styles.
*/
function ecommerce_gem_scripts() {

	wp_enqueue_style( 'ecommerce-gem-fonts', ecommerce_gem_fonts_url(), array(), null );

	wp_enqueue_style( 'jquery-meanmenu', get_template_directory_uri() . '/assets/third-party/meanmenu/meanmenu.css' );

	wp_enqueue_style( 'jquery-slick', get_template_directory_uri() . '/assets/third-party/slick/slick.css', '', '1.6.0' );

	wp_enqueue_style( 'ecommerce-gem-icons', get_template_directory_uri() . '/assets/third-party/et-line/css/icons.css', '', '1.0.0' );

	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/third-party/font-awesome/css/font-awesome.min.css', '', '4.7.0' );

	wp_enqueue_style( 'ecommerce-gem-style', get_stylesheet_uri() );

	wp_enqueue_script( 'ecommerce-gem-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ecommerce-gem-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'jquery-meanmenu', get_template_directory_uri() . '/assets/third-party/meanmenu/jquery.meanmenu.js', array('jquery'), '2.0.2', true );

	wp_enqueue_script( 'jquery-slick', get_template_directory_uri() . '/assets/third-party/slick/slick.js', array('jquery'), '1.6.0', true );

	// Add script for sticky sidebar.
	$sticky_sidebar = ecommerce_gem_get_option( 'enable_sticky_sidebar' );

	if( 1 == $sticky_sidebar ){

		wp_enqueue_script( 'jquery-theia-sticky-sidebar', get_template_directory_uri() . '/assets/third-party/theia-sticky-sidebar/theia-sticky-sidebar.min.js', array('jquery'), '1.0.7', true );

	}

	wp_enqueue_script( 'ecommerce-gem-custom', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), '2.0.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ecommerce_gem_scripts' );

/**
* Enqueue scripts and styles for admin >> widget page only.
*/
function ecommerce_gem_admin_scripts( $hook ) {

	if( 'widgets.php' === $hook ){

		wp_enqueue_style( 'ecommerce-gem-admin', get_template_directory_uri() . '/includes/widgets/css/admin.css', array(), '2.0.0' );

		wp_enqueue_media();

		wp_enqueue_script( 'ecommerce-gem-admin', get_template_directory_uri() . '/includes/widgets/js/admin.js', array( 'jquery' ), '2.0.0' );

	}

	if( is_admin() ) {
	    wp_enqueue_style( 'wp-color-picker' );
	    wp_enqueue_script( 'wp-color-picker' );
	}

}

add_action( 'admin_enqueue_scripts', 'ecommerce_gem_admin_scripts' );

// Load main file.
require_once trailingslashit( get_template_directory() ) . '/includes/main.php';

add_action( 'template_redirect', 'checkout_redirect_non_logged_to_login_access');
function checkout_redirect_non_logged_to_login_access() {

    // Here the conditions (woocommerce checkout page and unlogged user)
    if( is_checkout() && !is_user_logged_in()){

        // Redirecting to your custom login area
        wp_redirect( get_permalink( get_option('woocommerce_myaccount_page_id') ) );

        // always use exit after wp_redirect() function.
        exit;
    }
}
add_action( 'template_redirect', 'cart_redirect_non_logged_to_login_access');
function cart_redirect_non_logged_to_login_access() {

    // Here the conditions (woocommerce checkout page and unlogged user)
    if( is_cart() && !is_user_logged_in()){

        // Redirecting to your custom login area
        wp_redirect( get_permalink( get_option('woocommerce_myaccount_page_id') ) );

        // always use exit after wp_redirect() function.
        exit;
    }
}


add_action('wp_head','hide_order_menu');

function hide_order_menu() {
    if ( is_user_logged_in() ) {

    } else {
		$output="<style> #menu-item-243 { display: none; } </style>";


    }
    echo $output;
}

add_action ('woocommerce_thankyou' , 'send_order_data_to_api' , 10 , 1);
function send_order_data_to_api ($order_id) {

$order = new WC_Order( $order_id );
    $email = $order->billing_email;
    $phone = $order->billing_phone;
    $shipping_type = $order->get_shipping_method();
    $shipping_cost = $order->get_total_shipping();
		$user_id = $order->user_id;
	     $address_fields = array('country',
	         'title',
	         'first_name',
	         'last_name',
	         'company',
	         'address_1',
	         'address_2',
	         'address_3',
	         'address_4',
	         'city',
	         'state',
	         'postcode');

	     $address = array();
	     if(is_array($address_fields)){
	         foreach($address_fields as $field){
	             $address['billing_'.$field] = get_user_meta( $user_id, 'billing_'.$field, true );
	             $address['shipping_'.$field] = get_user_meta( $user_id, 'shipping_'.$field, true );
	         }
	     }
			 $items = $order->get_items();

	 $item_name = array();
	 $item_qty = array();
	 $item_price = array();
	 $item_sku = array();
	  $item_id= array();

	 foreach( $items as $key => $item){
			 $item_name[] = $item['name'];
			 $item_qty[] = $item['qty'];
			 $item_price[] = $item['line_total'];

			 $item_id[] = $item['product_id'];

	 }


	  $data = array('id' => $order_id,
        'src_lat' => 555,
        'src_long' => 888,
        'dest_lat' => 6666,
        'dest_long' => 888,
        'user_name'=> $address['shipping_first_name'],
        'phone' => $phone,
        'src_address' => 'ghbn',
        'dest_address' => $address['shipping_address_1'],
        'item_id' =>implode(',', $item_id),
        'item_name'=> implode(',', $item_name) ,
					 );
					 $headers=array('Content-Type: application/json',
					 								'Accept: application/json',
				 									'key: ecomm'
												);
	 						$content = json_encode($data);

				$curl = curl_init('http://127.0.0.1:8000/api/orders');
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER,$headers );
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

	$json_response = curl_exec($curl);

	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	var_dump($data);


	if ( $status != 201 ) {
	    die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
	}

	curl_close($curl);

}

add_action('rest_api_init', function () {

	register_rest_route( 'order/v3', '/track', array (//url is http://egycrafts.local/wp-json/order/v3/track
		'methods' => WP_REST_Server::EDITABLE,
		'callback' => 'order_track_func',
	));
} );
function order_track_func (WP_REST_Request $request){
	$order_id=  $request['order_id'];
	$new_status=$request['is_picked'];
	 $order = new WC_Order( $order_id );
	 $old_status=$order->status;
	 if($new_status==1){
		 $order->update_status('wc-pending');

	 }

	 return "success";

}


add_action('rest_api_init', function () {

	register_rest_route( 'order/v3', '/deliver', array (//url is http://egycrafts.local/wp-json/order/v3/deliver
		'methods' => WP_REST_Server::EDITABLE,
		'callback' => 'deliver_track_func',
	));
} );
function deliver_track_func (WP_REST_Request $request){
	$order_id=  $request['order_id'];
	$new_status=$request['is_delivered'];
	 $order = new WC_Order( $order_id );
	 $old_status=$order->status;
	 if($new_status==1){
		 $order->update_status('wc-completed');

	 }

	 return "success";

}

<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'egycrafts');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'brehan');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '08~x,D}GY{ ohep}Iz%cT1&h1A.qLq2cV!VA5#}Rc~S&Bb9F;w6QoP{1;=.OJ+jG');
define('SECURE_AUTH_KEY',  '[4MfD4N2PUy=0Foj!7mTf+$`l_8wbBU?|W4hK9)[srzbQMjD%{B In8 -p`yRBwG');
define('LOGGED_IN_KEY',    '(qm!=T$EX4n~CHN^pD3S8lB_K;2$z Fh-?+|mRJ[B:`VwnxhaV*v!Jfu4-3$cp`&');
define('NONCE_KEY',        ')+@N;u&-3 t*kiDBJ?QRKc_+ZX.5-b1,m n6mHJ:QYG)G-}WR[T2J.@+]HoA0AX<');
define('AUTH_SALT',        'H7z(6YnjY{>iZ Y8<>i;x/&V9pUf]o@2%tnGu @C8(jG@Ye{t5f)0#glK66Vpn.8');
define('SECURE_AUTH_SALT', '1}Pl`2e9n4MB~P$$$M*`-G5n3p8*%#k!$u~EK!X(Mt=uY2wF2B2J,<*#}/=yY2><');
define('LOGGED_IN_SALT',   '1#}zItOF|>j]F-m2AE~p{8ioCMDh3)K`oYkI}8B?TmwY&oo!WvFlsQ@*gewwnaOT');
define('NONCE_SALT',       ':lpbVEzhdkPNqN~s6oE]@aC0qq1i7yH|s=@jW1o>{8/0` FR/t<Vw>_1x44j|A7T');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
